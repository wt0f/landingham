FROM ruby:2.7.1-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client build-essential libsqlite3-dev

WORKDIR /usr/src/app
COPY . .
RUN bundle install

FROM ruby:2.7.1-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client libsqlite3-dev \
        taskwarrior \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY support/taskrc /root/.taskrc
COPY support/entrypoint.sh /entrypoint.sh
COPY --from=0 /usr/src/app/ /usr/src/app/
COPY --from=0 /usr/local/bundle/ /usr/local/bundle/

EXPOSE 3000
CMD ["/entrypoint.sh"]
