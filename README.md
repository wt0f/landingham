# Landingham

Workflow automation assistant


[![pipeline status](https://gitlab.com/wt0f/landingham/badges/master/pipeline.svg)](https://gitlab.com/wt0f/landingham/-/commits/master)
[![coverage report](https://gitlab.com/wt0f/landingham/badges/master/coverage.svg)](https://gitlab.com/wt0f/landingham/-/commits/master)

Provide a number of automated reporting functions by receiving webhooks from
various workflow systems.

Why Landingham?
---------------

Mrs. Landingham is the executive secretary of President Josiah Bartlet in the
first two seasons of The West Wing. She has a special ‘big-sister’ relationship
with Bartlet and is one of the only staffers who gets away with addressing him
in a blunt, informal manner.

------
This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
