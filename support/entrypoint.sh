#!/bin/bash

export RAILS_ENV=${RAILS_ENV-development}

rails db:migrate

rails server -b 0.0.0.0
